This simple node.js app provides a rest endpoint that converts a given numeric
string into a list of corresponding words in the style of [T9](https://en.wikipedia.org/wiki/T9_(predictive_text)).

In order to try the app run these commands in the root folder:
* 'npm install' to get the dependencies
* 'node app.js' to start the server
