const express = require("express");
const app = express();

//enable cross origin resource sharing to be able to run both api and client on localhost
const cors = require("cors");
app.use(cors());

//fake in-memory DB using a csv file
const csv = require("csv-parser");
const fs = require("fs");
const words = [];

//read the file with most common english words and keep it in an array
fs.createReadStream("dictionary.csv")
  .pipe(csv({ separator: " " }))
  .on("data", row => {
    words.push({
      id: parseInt(row.id),
      frequency: parseInt(row.frequency),
      word: row.word
    });
  })
  .on("end", () => {
    console.log("CSV file successfully processed");
    console.log(words[0]);
  });

const keypad = {
  2: ["a", "b", "c"],
  3: ["d", "e", "f"],
  4: ["g", "h", "i"],
  5: ["j", "k", "l"],
  6: ["m", "n", "o"],
  7: ["p", "q", "r", "s"],
  8: ["t", "u", "v"],
  9: ["w", "x", "y", "z"]
};

// recursive words generator - depricated
// const generateWords = keys => {
//   if (keys.length <= 1) return keys[0];

//   let words = [];
//   const [head, ...tail] = keys;
//   head.forEach(h => {
//     let newWords = generateWords(tail).map(w => `${h}${w}`);
//     words = [...words, ...newWords];
//   });

//   return words;
// };

//generates words as cartesian product of letter groups in 'keys'
//returns array of generated words
//'keys' is a two dimensional array, its elements are values from 'keypad' defined above
//iterative words generator - faster, reaches max callback if keys.length > 10
const generateWords = keys => {
  if (keys.length <= 1) return keys[0];
  let words = keys[0];
  //for each element of key in keys iterate already generated words and add the key to the end of the word
  keys.slice(1).forEach(k => {
    let newWords = [];
    k.forEach(x => {
      let g = words.map(w => `${w}${x}`);
      newWords.push(...g);
    });
    words = [...newWords];
  });
  return words;
};

//returns 'words' which match letters from elements in 'keys'
//if no 'words' are found, returns a single word created by joining first letters from elements in 'keys'
const getMatchingWords = keys => {
  let matching = words.filter(w => {
    if (w.word.length != keys.length) return false;
    return w.word
      .split("")
      .every((letter, index) => keys[index].includes(letter));
  });
  if (matching.length > 0) return matching.map(m => m.word);

  return [keys.reduce((acc, val) => (acc += val[0]), "")];
};

//word generator endpoint
//'textonym' - string of digits
//returns generated words
//returns 400 on wrong input, 500 if input is too long (see method 'generateWords')
app.get("/api/generator/:textonym", (req, res) => {
  const { textonym } = req.params;
  const isValid = /^[2-9]+$/.test(textonym);
  if (!isValid)
    return res.status(400).send("Textonym must be a sequence of digits [2-9].");

  const keys = textonym.split("").map(x => keypad[x]);

  if (keys.length > 10)
    return res
      .status(501)
      .send("Textonym is too long for current app version.");

  let suggestions = generateWords(keys);
  res.send(suggestions);
});

//words endpoint
//'textonym' - string of digits
//returns all words that are textonyms of 'textonym'
//returns 400 on wrong input
app.get("/api/words/:textonym", (req, res) => {
  const { textonym } = req.params;
  const isValid = /^[2-9]+$/.test(textonym);
  if (!isValid)
    return res.status(400).send("Textonym must be a sequence of digits [2-9].");

  const keys = textonym.split("").map(x => keypad[x]);

  let suggestions = getMatchingWords(keys);
  res.send(suggestions);
});

const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
